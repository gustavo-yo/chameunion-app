import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastroPage } from './cadastro.page';

//Componentes Helpper
import { ThemeModule } from 'src/theme/theme.module';

const routes: Routes = [
  {
    path: '',
    component: CadastroPage
  }
];

@NgModule({
  imports: [
    ThemeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CadastroPage]

})
export class CadastroPageModule {}
