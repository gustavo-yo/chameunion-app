import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

//Ionic
import { NavController, PopoverController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import AppGlobal from 'src/app/app.global';

//Firebase
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-innovations-popover',
  templateUrl: './innovations-popover.component.html',
  styleUrls: ['./innovations-popover.component.scss'],
})
export class InnovationsPopoverComponent implements OnInit {
  //var formulario reativo
	public innovationForm: FormGroup;
	//submit do form
	public submitted: boolean = false;


	public userId: any;

  constructor(
    public formbuilder: FormBuilder,
		public afAuth: AngularFireAuth,
		public alertController: AlertController,
		public loading: LoadingController,
		public translate: TranslateService,
		public popoverController: PopoverController,
		public toastController: ToastController

  ) { 
	

    this.innovationForm = this.formbuilder.group({
			id: null,
			titulo: [null, [Validators.required]],
			descricao: [null, [Validators.required]],
			arquivo: [null, [Validators.required]]
		})
  }

  ngOnInit() {
	this.start();
  }

  async start(){
	firebase.auth().onAuthStateChanged((user) => {
		if (user) {
		  this.userId = user.uid;
		} else {
		}
	  });
	}

  async submitInnovation() {

	this.submitted = true;
		let usuario: any = Object.assign({}, this.innovationForm.value);
		usuario.id;
		usuario.titulo;
		usuario.descricao;
		usuario.arquivo;
		usuario.status = 'Em análise'; 

		if (usuario.titulo == null) {			
			const toast = await this.toastController.create({
				message: 'Preencha todos os campos para continuar!',
				duration: 2000
			  });
			  toast.present();
		}else{
			firebase.database().ref('/app/open-innovation/' + this.userId).push().set({title: usuario.titulo, desc: usuario.descricao, archive: usuario.arquivo, status: usuario.status});
			this.popoverController.dismiss();
			const toast = await this.toastController.create({
				message: 'Projeto criado com sucesso!',
				duration: 2000
			  });
			  toast.present();
				location.reload(true);
		}

		
    }
}
