//Angular
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

//Ionic
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

//Firebase
import * as firebase from 'firebase';
import * as moment from 'moment';
import { AngularFireAuth } from '@angular/fire/auth';

//Service Firebase
import { FirebaseService } from '../../service/firebase.service';

import AppGlobal from '../app.global';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

	//var formulario reativo
	public loginForm: FormGroup;

	//submit do form
	public submitted: boolean = false;

	//Mostra a senha do inout
	public iconeye = 'eye';

	//type do campo de senha
	public password = 'password';

	constructor(
		public formbuilder: FormBuilder,
		public afAuth: AngularFireAuth,
		private navigation: NavController,
		public alertController: AlertController,
		public loading: LoadingController,
		public firebaseErros: FirebaseService,
		public translate: TranslateService
	) {
		this.loginForm = this.formbuilder.group({
			email: [null, [Validators.required, Validators.email]],
			password: [null, [Validators.required, Validators.minLength(6)]],
		});
	}

	ngOnInit() {

	}

	//Alert Padrão
	async showAlert(title: string, msg: string, btn: string) {
		const alert = await this.alertController.create({
			subHeader: title,
			message: msg,
			buttons: [btn]
		});

		await alert.present();
	}

	//Login
	submitLogin() {

		this.submitted = true;

		//loading enquanto faz a requisição
		this.loading.create({ message: this.translate.instant('wait') + "..." }).then(_alert => {
			_alert.present();
		});

		//Faz o login
		this.afAuth.auth.signInWithEmailAndPassword(
			this.loginForm.value.email, this.loginForm.value.password).then((response) => {

				// Busca dados usuário
				this.getUserInDatabase(response.user.uid);
			})
			.catch((error) => {
				console.log('error', error);
				//verifica se existe o erro no serviço de erros do firebase
				const ErrosFirebase = this.firebaseErros.errosLogin().filter(item => (item.id == error.code));

				if (error.code == ErrosFirebase[0].id) {

					//Mostra a mensagem de erro do Firebase
					this.showAlert('', ErrosFirebase[0].name, 'Voltar');

					//reseta o input de senha
					this.loginForm.controls['password'].setValue(null);

					//finaliza o loading
					this.loading.dismiss();
				} else {
					//Mensagem de Erro padrão caso não encontre o erro de firesabe
					this.showAlert('', this.translate.instant('invalid_user_pass'), 'ok');

					//reseta o input de senha
					this.loginForm.controls['password'].setValue(null);

					//Finaliza o loading
					this.loading.dismiss();
				}
			})
		//finaliza o loading
		this.loading.dismiss();
	}

	// Busca dados do usuário
	public getUserInDatabase(userId: string) {

		firebase.database().ref('app/usuarios').child(userId).once('value').then(snapshot => {

			let date = moment().format('YYYY-MM-DD HH:mm:ss');
			firebase.database().ref('/app/logs/' + userId +'/Login').push().set(date);

			if(snapshot.val() && snapshot.val().nome) {

				let data: object = Object.assign({}, snapshot.val());

				// Se não tiver idioma pré cadastrado captura o idioma do cliente
				if(!data['idioma']) {

					data['idioma'] = this.translate.defaultLang;

					// salva valor
					firebase.database().ref('app/usuarios').child(userId).child('idioma').set(data['idioma']);
				}

				//Salva o usuário no storage
				AppGlobal.getInstance().setUser(data).then(() => {
					// seta linguagem
					this.translate.setDefaultLang(data['idioma']);

					//Enviar o usuário para página home
					this.navigation.navigateForward('/pages');
				})
			}
			else {
				//Mostra a mensagem de erro do Firebase
				this.showAlert('', this.translate.instant('notfound_user'), 'Voltar');
			}

			//finaliza o loading
			this.loading.dismiss();
		});
	}

	public recoverPassword() {
		//Enviar o usuário para página home
		this.navigation.navigateForward('/recover');
	}

	//Mostra senha para o usuário
	public eyeTroca(event) {
		if (event.target.name == 'eye') {
			this.iconeye = 'eye-off';
			this.password = 'text';
		} else {
			this.iconeye = 'eye';
			this.password = 'password';
		}
	}

}
