import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThemeModule } from '../../theme/theme.module';

import { StartPage } from './start.page';


const routes: Routes = [
  {
    path: '',
    component: StartPage
  }
];

@NgModule({
  imports: [
    ThemeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StartPage],
  exports: [
    StartPage
  ]
})
export class StartPageModule {}
