import { Storage } from '@ionic/storage';
import { Injector } from '@angular/core';


export default class AppGlobal {

    private static __instance: AppGlobal = new AppGlobal();
    private storage: Storage;

    constructor() {
        if(AppGlobal.__instance){
			throw new Error("Error: Use AppGlobal.getInstance() no lugar de `new AppGlobal()`.");
		}

        // Dependencia
        const injector = Injector.create({ providers: [{provide: Storage, deps:[]}] });
        this.storage = injector.get(Storage);
    }

    public static getInstance(): AppGlobal {
        return AppGlobal.__instance;
    }

    public async getUser() {
        return await this.storage.get('user');
    }

    public async setUser(user: any) {
        return await this.storage.set('user', user);
    }

    public async getFile(fileId) {
        let files: any = await this.storage.get('downloads');

        if(!files) {
            await this.storage.set('downloads', {});
            return null;
        }

        if(files[fileId])
            return files[fileId];

        return null;
    }

    public async setFile(fileUrl: any, fileId: string) {
        let files: any = await this.storage.get('downloads');

        if(!files) files = {};

        files[fileId] = fileUrl;

        return await this.storage.set('downloads', files);
    }

    public async deleteFile(fileId: string) {
        let files: any = await this.storage.get('downloads');
        delete files[fileId];
        return await this.storage.set('downloads', files);
    }
}
