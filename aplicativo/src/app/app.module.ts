//Angular
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';

// Ionic
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { Firebase } from '@ionic-native/firebase/ngx'
import { CallNumber } from '@ionic-native/call-number/ngx';

// General
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// Fire
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';

// Arquivo Environment
import { environment } from '../environments/environment';

//Storage
import { IonicStorageModule } from '@ionic/storage';

import { StartPage } from './start/start.page';
import { StartPageModule } from './start/start.module';
import { ThemeModule } from '../theme/theme.module';
import { WebView } from '@ionic-native/ionic-webview/ngx';

// Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// popover
import { NotificationsComponent } from './components/notifications/notifications.component';
import { InnovationsPopoverComponent } from './components/innovations-popover/innovations-popover.component';

// required for AOT compilation
export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http);
}




@NgModule({
    declarations: [AppComponent, NotificationsComponent, InnovationsPopoverComponent  ],
    entryComponents: [
        StartPage, NotificationsComponent, InnovationsPopoverComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        IonicStorageModule.forRoot(),

        ThemeModule,

        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,

        StartPageModule,

        HttpClientModule,
        TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient]
			}
		})
    ],
    providers: [
		Camera,
        StatusBar,
        SplashScreen,
        FileTransfer,
        File,
        WebView,
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        Firebase,
        CallNumber
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
