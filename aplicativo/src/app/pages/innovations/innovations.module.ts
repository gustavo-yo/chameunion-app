import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThemeModule } from 'src/theme/theme.module';

import { InnovationsPage } from './innovations.page';

const routes: Routes = [
  {
    path: '',
    component: InnovationsPage
  }
];

@NgModule({
  imports: [
    ThemeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InnovationsPage]
})
export class InnovationsPageModule {}
