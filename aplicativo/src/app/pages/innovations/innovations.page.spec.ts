import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnovationsPage } from './innovations.page';

describe('InnovationsPage', () => {
  let component: InnovationsPage;
  let fixture: ComponentFixture<InnovationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnovationsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnovationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
