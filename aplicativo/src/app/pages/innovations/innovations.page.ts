import { Component, OnInit } from '@angular/core';
import { InnovationsPopoverComponent } from 'src/app/components/innovations-popover/innovations-popover.component';
import { PopoverController, NavController } from '@ionic/angular';
import AppGlobal from 'src/app/app.global';
import * as firebase from 'firebase';

@Component({
  selector: 'app-innovations',
  templateUrl: './innovations.page.html',
  styleUrls: ['./innovations.page.scss'],
})
export class InnovationsPage implements OnInit {

  public text: string;
  public products: any;
  public allProducts: any;
  public innovations: Array<any>;
  public allInnovations: any;
  public key: any;

  constructor(public popoverCtrl: PopoverController, private navigation: NavController) {
    this.text = '<p>Carregando...</p>';
  }

  ngOnInit() {
    this.loadData();
    this.start();
  }

  public async start() {
    
      const user: any = await AppGlobal.getInstance().getUser();
      firebase.database().ref('app/open-innovation').child(user.id).once('value').then(snapshot => {

        this.products = snapshot.val();
        this.allProducts = Object.values(this.products);
        
        this.key = Object.keys (this.products);
        
        console.log(this.key);
        console.log(this.products);
        console.log(this.allProducts);
      });
  }

  private async loadData() {
    //     const user: any = await AppGlobal.getInstance().getUser();
    //     firebase.database().ref('app/sobre').child(user.idioma).once('value').then(snapshot => {
    this.text = 'Compartilhe seu projeto com a Chemyunion, ou então solicite um serviço junto ao time de inovação.';
    //     });
  }

  async newInnovation(ev: any) {
    const popover = await this.popoverCtrl.create({
      component: InnovationsPopoverComponent,
      event: ev,
      animated: true,
      showBackdrop: true,
      cssClass: "popover"
    });
    return await popover.present();
  }

  public openDetail(id: string) {
    this.navigation.navigateForward('/pages/menu/docs/innovation-detail', { animated: true });
  }

}
