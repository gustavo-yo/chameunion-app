import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import AppGlobal from 'src/app/app.global';
import * as firebase from 'firebase/app';
import 'firebase/database';

@Component({
    selector: 'app-products',
    templateUrl: './products.page.html',
    styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

    public products: object;
    public allProducts: any;
    public productList: Array<any>;
    public loading: boolean;
    public searchTerm: any;
    public fullList: Array<any>;
    public userId: any;
    

    constructor(private navigation: NavController) {
        this.loading = false;
    }

    ngOnInit() {
        this.start();
    }

    public async start() {
        this.loading = true;
        const user: any = await AppGlobal.getInstance().getUser();
        firebase.database().ref('app/produtos/itens').child(user.idioma).once('value').then(snapshot => {
            this.products = snapshot.val();
            this.allProducts = this.products;
            this.loading = false;     
            this.userId = user.id;

            console.log(this.allProducts);
            
        });
    }

    public segmentChanged(event: any) {
        if(this.products && this.products[event.detail.value]) {
            let items: Array<any> = [];
            for(const key of Object.keys(this.products[event.detail.value])) {
                const item = Object.assign({}, this.products[event.detail.value][key]);
                item.id = key;
                items.push(item);
            }

            // sort
            items.sort((a, b) => {
                if (a.title > b.title) {
                    return 1;
                }
                if (a.title < b.title) {
                    return -1;
                }
                return 0;
            });

            this.productList = items;
            this.fullList = items;
            
        }
    }

    public openDetail(item: any) {
        this.navigation.navigateForward('/pages/menu/docs/products/' + item.id + '/' + item.title, {animated: true});
        
        firebase.database().ref('/app/logs/' + this.userId +'/Product').push().set(item.title);
    }

    setFilteredItems(searchTerm: any){
        let val = searchTerm.target.value;

        if (val && val.trim() != ""){
            this.productList = this.productList.filter((produto) => {
                return(produto.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }else{
            this.productList = this.fullList;
            
        }
    }  
    
}


