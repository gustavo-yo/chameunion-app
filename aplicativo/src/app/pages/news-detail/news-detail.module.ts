import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsDetailPage } from './news-detail.page';
import { ThemeModule } from 'src/theme/theme.module';

const routes: Routes = [
    {
        path: '',
        component: NewsDetailPage
    }
];

@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [NewsDetailPage]
})
export class NewsDetailPageModule {}
