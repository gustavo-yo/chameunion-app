import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as firebase from 'firebase';
import AppGlobal from 'src/app/app.global';
import { FirebaseService } from 'src/service/firebase.service';

@Component({
    selector: 'app-news-detail',
    templateUrl: './news-detail.page.html',
    styleUrls: ['./news-detail.page.scss'],
})
export class NewsDetailPage implements OnInit {

    public loading: boolean;
    public news: any;

    constructor(public fbService: FirebaseService,private route: ActivatedRoute) { }

    ngOnInit() {
        this.loading = true;
    }

    public ionViewWillEnter() {
        this.loadData();
    }

    private async loadData() {
        const user: any = await AppGlobal.getInstance().getUser();
        const newsId: string = this.route.snapshot.paramMap.get('newsId');
        const categoryId: string = this.route.snapshot.paramMap.get('categoryId');

        let ref;
        if(categoryId == 'desc')
            ref = firebase.database().ref('app/destaques').child(user.idioma).child(newsId);
        else
            ref = firebase.database().ref('app/noticias').child(user.perfil_id).child(user.idioma).child(categoryId).child(newsId);

        ref.once('value').then(snapshot => {
			this.news = Object.assign({}, snapshot.val());
            this.fbService.incrementItem('views', this.news, ref, String(new Date().getTime()));
            this.loading = false;
		});
    }
}
