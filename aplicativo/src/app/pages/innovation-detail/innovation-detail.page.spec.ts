import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnovationDetailPage } from './innovation-detail.page';

describe('InnovationDetailPage', () => {
  let component: InnovationDetailPage;
  let fixture: ComponentFixture<InnovationDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnovationDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnovationDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
