import { Component, OnInit } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx'; //Ionic-native está funcionando em prod
import { ActionSheetController, NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import AppGlobal from 'src/app/app.global';

import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.page.html',
	styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

	//var formulario reativo
	public profileForm: FormGroup;
	public base64Image: any = '../../../assets/images/default-avatar.png';
	public loading: boolean = false;

	constructor(
		private formbuilder: FormBuilder,
		private camera: Camera,
		public alertController: AlertController,
		public actionSheetController: ActionSheetController,
		private navigation: NavController,
		public firebaseAuth: AngularFireAuth,
		public translate: TranslateService
	) {
		this.profileForm = this.formbuilder.group({
			nome: ['', [Validators.required, Validators.minLength(3)]],
			localizacao: ['', [Validators.required, Validators.minLength(5)]],
			data_nascimento: ['', [Validators.required, Validators.minLength(6)]],
			telefone: ['', [Validators.minLength(11)]],
			empresa: ['', [Validators.minLength(1)]],
			email: ['', [Validators.required, Validators.minLength(11)]],
			password: ['', [Validators.minLength(11)]],
			idioma: [''],
			selectNews: ['']
		});
	}

	ngOnInit() {
		this.start();
	}

	public async start() {
		const userData: any = await AppGlobal.getInstance().getUser();

		if(userData.foto) {
			this.base64Image = userData.foto;
		}

		this.profileForm.setValue({
			nome: userData.nome || '',
			localizacao: userData.localizacao || '',
			data_nascimento: userData.data_nascimento || '',
			telefone: userData.telefone || '',
			empresa: userData.empresa || '',
			email: userData.email || '',
			password: userData.password || '',
			idioma: userData.idioma || '',
			selectNews: userData.selectNews || '',
		});
	}

	public setLanguage(language: string) {
		this.profileForm.controls.idioma.setValue(language);
		this.profileForm.controls.selectNews.setValue(null);
	}

	public openNews() {
		this.navigation.navigateForward('pages/menu/favnews', {animated: true});
	}

	public async submitLogin() {
		if(this.profileForm.valid) {
			this.translate.setDefaultLang(this.profileForm.value.idioma);
			firebase.database().ref('app/usuarios/' + this.firebaseAuth.auth.currentUser.uid).update(this.profileForm.value);
			AppGlobal.getInstance().setUser(Object.assign(await AppGlobal.getInstance().getUser(), this.profileForm.value));

			const alert: any = await this.alertController.create({
				message: 'Dados salvos!',
				buttons: ['Ok']
			});
			alert.present();
		}
		else {
			const alert: any = await this.alertController.create({
				subHeader: 'Atenção',
				message: 'Verifique os dados',
				buttons: ['Ok']
			});

			alert.present();
		}
	}

	public async presentActionSheet() {
		const actionSheet = await this.actionSheetController.create({
			buttons: [{
				text: 'Tirar foto',
				icon: 'camera',
				handler: () => {
					this.takephoto();
				}
			}, {
				text: 'Escolha foto da galeria',
				icon: 'photos',
				handler: () => {
					this.openGallery();
				}
			}]
		});
		await actionSheet.present();
	}

	private __callPhoto(only_gallery = false){

		let options = {
			quality: 70,
			destinationType: this.camera.DestinationType.DATA_URL,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true,
			targetWidth:720 
		}

		if(only_gallery)
			options['sourceType'] = this.camera.PictureSourceType.SAVEDPHOTOALBUM;

		this.camera.getPicture(options).then((imageData) => {
			console.log(imageData);
			let imageBase64 = 'data:image/jpeg;base64,' + imageData;
			this.uploadImage(imageBase64);
		}).catch((error) => {
			console.error(error);
		});
	}

	public takephoto() {
		this.__callPhoto();
	}

	public openGallery() {

		this.__callPhoto(true);
	}

	public uploadImage(image) {
		this.loading = true;
		const ref = firebase.storage().ref('profiles/' + this.firebaseAuth.auth.currentUser.uid + '.jpeg');
		ref.putString(image, 'data_url').then(async _snapshot => {
			ref.getDownloadURL().then(async url =>{

				firebase.database().ref('app/usuarios/' + this.firebaseAuth.auth.currentUser.uid + '/foto').set(url);
				AppGlobal.getInstance().setUser(Object.assign(await AppGlobal.getInstance().getUser(), {foto: url}));

				this.base64Image = url;
				this.loading = false;
			});
		});
	}
}
