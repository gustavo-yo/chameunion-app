import { NgModule } from '@angular/core';

import { PagesPage } from './pages.page';
import { ThemeModule } from 'src/theme/theme.module';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    {
        path: 'menu',
        component: PagesPage,
        children: [
            { path: 'home', loadChildren: './home/home.module#HomePageModule' },
            { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
            { path: 'docs/media', loadChildren: './midia/midia.module#MidiaPageModule' },
            { path: 'docs/media/:id', loadChildren: './media-detail/media-detail.module#MediaDetailPageModule' },
            { path: 'docs/products', loadChildren: './products/products.module#ProductsPageModule' },
            { path: 'docs/products/:id/:title', loadChildren: './product-detail/product-detail.module#ProductDetailPageModule' },
            { path: 'news', loadChildren: './news/news.module#NewsPageModule' },
            { path: 'news/:categoryId/:newsId', loadChildren: './news-detail/news-detail.module#NewsDetailPageModule' },
            { path: 'account', loadChildren: './profile/profile.module#ProfilePageModule' },
            { path: 'favnews', loadChildren: './fav-news/fav-news.module#FavNewsPageModule' },
            { path: 'my-orders', loadChildren: './my-orders/my-orders.module#MyOrdersPageModule' },
            { path: 'innovations', loadChildren: './innovations/innovations.module#InnovationsPageModule' },
            { path: 'order-detail', loadChildren: './order-detail/order-detail.module#OrderDetailPageModule' },
            { path: 'contact', loadChildren: './contact/contact.module#ContactPageModule' },
            { path: 'docs/innovation-detail/:id', loadChildren: './innovation-detail/innovation-detail.module#InnovationDetailPageModule' },

            { path: '', redirectTo: '/pages/menu/home'}
        ]
    },
    {
        path: '',
        redirectTo: '/pages/menu/home'
    }
]


@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [PagesPage]
})
export class PagesPageModule {}
