import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import AppGlobal from 'src/app/app.global';

@Component({
    selector: 'app-about',
    templateUrl: './about.page.html',
    styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

    public text: string;

    constructor() {
        this.text = '<p>Carregando...</p>';
    }

    ngOnInit() {
        this.loadData();
    }

    private async loadData() {
        const user: any = await AppGlobal.getInstance().getUser();
        firebase.database().ref('app/sobre').child(user.idioma).once('value').then(snapshot => {
            this.text = snapshot.val();
        });
    }
}
