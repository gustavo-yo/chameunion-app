import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutPage } from './about.page';
import { ThemeModule } from '../../../theme/theme.module';

const routes: Routes = [
    {
        path: '',
        component: AboutPage 
    }
];

@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [AboutPage]
})
export class AboutPageModule {}
