import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

import * as firebase from 'firebase/app';
import 'firebase/database';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.page.html',
  styleUrls: ['./my-orders.page.scss'],
})
export class MyOrdersPage implements OnInit {

  public products: object;
  public fullList: any;
  public productList: Array<any>;

  constructor(private navigation: NavController) {
    

  }

  ngOnInit() {
}
public openOrder(){
  this.navigation.navigateForward('/pages/menu/order-detail', {animated: true});
}

  public segmentChanged(event: any) {
    if(this.products && this.products[event.detail.value]) {
        let items: Array<any> = [];
        for(const key of Object.keys(this.products[event.detail.value])) {
            const item = Object.assign({}, this.products[event.detail.value][key]);
            item.id = key;
            items.push(item);
        }

        // sort
        items.sort((a, b) => {
            if (a.title > b.title) {
                return 1;
            }
            if (a.title < b.title) {
                return -1;
            }
            return 0;
        });

        this.productList = items;
        this.fullList = items;
        
    }
  }

}
