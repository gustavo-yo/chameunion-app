import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as firebase from 'firebase';
import { FirebaseService } from 'src/service/firebase.service';

@Component({
    selector: 'app-product-detail',
    templateUrl: './product-detail.page.html',
    styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {

    public loading: boolean;
    public detail: any;
    public title: string;

    constructor(public fbService: FirebaseService,private route: ActivatedRoute) { }

    ngOnInit() {
        this.loading = true;
        this.title = '';
    }

    public ionViewWillEnter() {
        this.loadData();
    }

    private async loadData() {
        const detailId: string = this.route.snapshot.paramMap.get('id');
        this.title = this.route.snapshot.paramMap.get('title');

        let ref = firebase.database().ref('app/produtos/itens/pt/cabelo/').child(detailId);
        ref.once('value').then(snapshot => {
			this.detail = Object.assign({}, snapshot.val());
            this.loading = false;
		});
    }
}
