import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import AppGlobal from 'src/app/app.global';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-midia',
    templateUrl: './midia.page.html',
    styleUrls: ['./midia.page.scss'],
})
export class MidiaPage implements OnInit {

    public loading: boolean;
    public categories: Array<object>;

    constructor(public firebaseAuth: AngularFireAuth, private navigation: NavController) {
        this.loading = false;
        this.categories = [];
    }

    ngOnInit() {
        this.loading = true;
        this.start();
    }

    public async start() {
        const user: any = await AppGlobal.getInstance().getUser();
        firebase.database().ref('app/docs/midia').child(user.perfil_id).child(user.idioma).orderByChild('label').once('value').then(snapshot => {
            snapshot.forEach(child => {
                let item: Object = child.val();
                item['open'] = false;

                // ordena valores
                let keys: Array<string> = Object.keys(item['arquivos']).sort((key1, key2) => {
                    if (item['arquivos'][key1] > item['arquivos'][key2]) return 1; // key1 > key2
                    if (item['arquivos'][key1] < item['arquivos'][key2]) return -1; // key1 < key2
                    return 0; // key1 must be equal to key2
                });
                keys = keys.reverse();

                item['arquivos_load'] = [];

                // cria array de arquivos
                for(let key of keys) {
                    item['arquivos_load'].push({loading: true, id: key});

                    // Carrega noticia
                    firebase.database().ref('app/midia').child(key).once('value').then(fileSnap => {

                        let fileRef: any = item['arquivos_load'].find((item) => {
                            return item.id == fileSnap.key
                        })

                        if(fileRef) {
                            fileRef.loading = false;
                            Object.assign(fileRef, fileSnap.val());
                        }
                    });
                }

                // se não existir nada não exibe categoria
                if(item['arquivos_load'].length)
                    this.categories.push(item);
            });
            this.loading = false;
        });
    }

    public openCategory(category: object) {
        category['open'] = !category['open'];
    }

    public openDetail(id: string) {
        this.navigation.navigateForward('/pages/menu/docs/media/' + id, {animated: true});
    }

    public setDate(date): Date {
		return moment(date).toDate();
	}
}
