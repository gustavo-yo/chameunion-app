import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MidiaPage } from './midia.page';
import { ThemeModule } from 'src/theme/theme.module';

const routes: Routes = [
    {
        path: '',
        component: MidiaPage
    }
];

@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [MidiaPage]
})
export class MidiaPageModule {}
