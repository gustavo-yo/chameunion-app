import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { FavNewsPage } from './fav-news.page';
import { ThemeModule } from 'src/theme/theme.module';

const routes: Routes = [
    {
        path: '',
        component: FavNewsPage
    }
];

@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [FavNewsPage]
})
export class FavNewsPageModule {}
