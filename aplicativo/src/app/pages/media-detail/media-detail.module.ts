import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MediaDetailPage } from './media-detail.page';
import { ThemeModule } from 'src/theme/theme.module';

const routes: Routes = [
    {
        path: '',
        component: MediaDetailPage
    }
];

@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild(routes)
    ],
    declarations: [MediaDetailPage]
})
export class MediaDetailPageModule {}
