import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ThemeModule } from '../../../theme/theme.module';

import { HomePage } from './home.page';

@NgModule({
    imports: [
        ThemeModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage
            }
        ])
    ],
    declarations: [
        HomePage
    ],
    exports: [
        HomePage
    ]
})
export class HomePageModule {}
