import { Component, ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import AppGlobal from 'src/app/app.global';
import * as firebase from 'firebase';
import * as moment from 'moment';
import { NavController, PopoverController } from '@ionic/angular';
import { FirebaseService } from 'src/service/firebase.service';

import { NotificationsComponent } from './../../components/notifications/notifications.component';
import { Firebase } from '@ionic-native/firebase/ngx';
// import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage {

	@ViewChild(Slides) slides: Slides;

	public highlights: Array<any>;
	public lastNews: Array<any>;
	public fullNews: Array<any>;
	public lastNewsData: any;
	public userId: any;

	sliderConfig = {
		slidesPerView: 1.6,
		spaceBetween: 10,
		centeredSlides: false
	};

	sliderConfiOpen = {
		slidesPerView: 2,
		spaceBetween: 10,
		centeredSlides: false
	};

	public tvCorporativa = {
		title: 'TV CORPORATIVA',
		text: 'Retenção de água no interior dos fios é a chave para proteger os cabelos',
		data: '7 SET',
		view: '350',
		amei: '25',
		img: 'pgina-inicial-com-rolagem-rectangle-copy.jpg'
	}

	constructor(
		public fbService: FirebaseService,
		private navigation: NavController,
		public popoverCtrl: PopoverController,
		private firebase: Firebase,
		) {
			this.firebase.setScreenName('HomeIonic');
		}

	ngOnInit() {
		this.start();
		
	}

	public async start() {

		const user: any = await AppGlobal.getInstance().getUser();
		
		firebase.database().ref('app/destaques').child(user.idioma).orderByChild('created_at').once('value').then(snapshot => {
			this.highlights = [];
			snapshot.forEach(child => {
				this.highlights.push(child.val());
				this.userId = user.id;
			});
		});


		firebase.database().ref('app/home').child(user.perfil_id).child(user.idioma).child('news').once('value').then(chave => {
			if(chave.val()) {
				let item = chave.val();
				this.lastNewsData = item;

				firebase.database().ref(item.link).orderByChild('created_at').once('value').then(snapshot => {
					this.lastNews = [];

					this.fullNews = this.lastNews;

					snapshot.forEach(child => {
						this.lastNews.unshift(Object.assign({}, child.val()));
					});
				});
			}
		});
	}

	public setDate(date): Date {
		return moment(date).toDate();
	}

	public openHighlight(news) {
		let stringExemplo = news.url_antiga;
		let resultado = stringExemplo.split ("/", 7);
		console.log(resultado);
		this.navigation.navigateForward('/pages/menu/news/desc/' + resultado[6], {animated: true});
	}

	public openNews(news) {
		if(news.id)
			this.navigation.navigateForward('/pages/menu/news/' + this.lastNewsData.categoria + '/' + news.id, {animated: true});
			firebase.database().ref('/app/logs/' + this.userId +'/News').push().set(news.id);
	}

	public openProducts(){
		this.navigation.navigateForward('/pages/menu/docs/products', {animated: true});
	}

	setFilteredItems(searchTerm: any){
        let val = searchTerm.target.value;
		console.log(this.lastNews);
        if (val && val.trim() != ""){
            this.lastNews = this.lastNews.filter((noticias) => {
                return(noticias.conteudo.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }else{
            this.lastNews = this.fullNews;
            
        }
	}
	
	async notifications(ev: any) {
		const popover = await this.popoverCtrl.create({
			component: NotificationsComponent,
			event: ev,
			animated: true,
			showBackdrop: true
		});
		return await popover.present();
	}
	
	
}
