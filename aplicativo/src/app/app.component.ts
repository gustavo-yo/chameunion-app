import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { TranslateService } from '@ngx-translate/core';

import { NavController } from '@ionic/angular';

//import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private router: Router,
        public translate: TranslateService,
        public navigation: NavController,
        private auth: AngularFireAuth,
        private storage: Storage,
    ) {
        // Verifica se existe na lista de linguagens
        let language: string;
        if(environment.languages.indexOf(navigator.language.split('-')[0]) != -1)
            language = navigator.language.split('-')[0];
        else
            language = environment.defaultLanguage;

        translate.setDefaultLang(language);

        this.initializeApp();
    }

    public async initializeApp() {
        this.platform.ready().then(async () => {
            this.statusBar.styleDefault();
            this.statusBar.backgroundColorByHexString('#00346E');
            this.statusBar.styleLightContent();

            const dataUser = await this.storage.get('user');
            if(dataUser && dataUser.idioma) {
                this.translate.setDefaultLang(dataUser.idioma);
            }

            this.auth.auth.onAuthStateChanged(user => {
                this.splashScreen.hide();
                if(user && this.router.url === '/start') this.navigation.navigateForward('/pages/menu/home', {animated: true});
            });
        });
    }
}
