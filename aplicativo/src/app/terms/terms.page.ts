import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import AppGlobal from 'src/app/app.global';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-terms',
    templateUrl: './terms.page.html',
    styleUrls: ['./terms.page.scss'],
})
export class TermsPage implements OnInit {

    public text: string;

    constructor(private translate: TranslateService) {
        this.text = '<p>Carregando...</p>';
    }

    ngOnInit() {
        this.loadData();
    }

    private async loadData() {
        let idioma = this.translate.defaultLang;

        const user: any = await AppGlobal.getInstance().getUser();
        firebase.database().ref('app/termos').child(idioma).once('value').then(snapshot => {
            this.text = snapshot.val();
        });
    }
}
