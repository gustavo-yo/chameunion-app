export const environment = {
    production: false,
    defaultLanguage: 'en',
    languages: ['pt', 'en'],
    s3: 'https://chemyunion-community.s3.amazonaws.com/files/folders/',
    firebase: {
        apiKey: "AIzaSyBeN-URfmNf7XHikiTOSS2NNw8_KJwW2Bo",
        authDomain: "community-homolog.firebaseapp.com",
        databaseURL: "https://community-homolog.firebaseio.com",
        projectId: "community-homolog",
        storageBucket: "community-homolog.appspot.com",
        messagingSenderId: "298008894081"
    }
};
