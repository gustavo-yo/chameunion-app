import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import * as moment from 'moment';

@Injectable({
	providedIn: 'root'
})
export class FirebaseService {

	public firebaeAuth:any;

	public firebaeLogin:any;

	constructor() { }

	public errosAuth() {

		this.firebaeAuth = [
			{ id: 'auth/email-already-in-use', name: 'O endereço de e-mail já está sendo usado por outra conta.' },
			{ id: 'auth/invalid-email', name: 'O endereço de e-mail é invalido' },
			{ id: 'auth/weak-password', name: 'Sua senha não é forte o suficiente' }
		];

		return this.firebaeAuth;
	}

	public errosLogin() {

		this.firebaeLogin = [
			{ id: 'auth/invalid-email', name: 'O endereço de e-mail não é válido' },
			{ id: 'auth/user-disabled', name: 'E-mail fornecido foi desativado' },
			{ id: 'auth/user-not-found', name: 'O endereço de e-mail não é válido' },
			{ id: 'auth/wrong-password', name: 'Senha inválida' }
		];

		return this.firebaeLogin;
	}

	public countItems(key: string, item: any): number {
		if(!item)
			return 0;

		if(!item[key])
			return 0;

		return Object.keys(item[key]).length;
	}

	public incrementItem(key: string, item: any, ref: firebase.database.Reference, value: string) {
		if(!item[key]) {
			item[key] = {};
		}

		if(!item[key][value]) {
			ref.child(key).child(value).set(true);
			item[key][value] = true;
		}
		else {
			ref.child(key).child(value).remove();
			delete item[key][value];
		}
	}

	public getDate(date): Date {
		return moment(date).toDate();
	}
}
