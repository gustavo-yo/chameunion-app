import { Component, Input } from '@angular/core';

@Component({
	selector: 'button-padrao',
	styleUrls: ['btn-padrao.scss'],
	template:
	`<ion-button color="primary" shape="round" translate>{{ label }}</ion-button>`
})
export class BtnPadraoComponent {
	/**
	 * @param label: string | Atributo html utilizado para adicionar uma mensagem dentro do botão | possuí uma mensagem default: 'Investir agora'
	 */
	@Input('label') label: string = 'Salvar';
}
