import { NotificationsComponent } from 'src/app/components/notifications/notifications.component';
import { PopoverController } from '@ionic/angular';


import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.html',
  styleUrls: ['./header.scss'],
})
export class HeaderComponent implements OnInit {

  constructor(  public popoverCtrl: PopoverController ) { }

  ngOnInit() {}

  async notifications(ev: any) {
    const popover = await this.popoverCtrl.create({
        component: NotificationsComponent,
        event: ev,
        animated: true,
        showBackdrop: true
    });
    return await popover.present();
}

}

